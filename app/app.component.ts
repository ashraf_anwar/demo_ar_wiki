import { Component } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { Wikitude } from "nativescript-wikitude";

declare var android;

registerElement("Wikitude", () => Wikitude);

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})

export class AppComponent {
    constructor() {
        (global as any).wikitudeLicense =
      "Atwe8qAew9p4rchLCSQDkTBcpK4ufXPCwea8p7CTvXS3WorNCJmTzCrmERJ6KF9/g8/rQI5g+bJcNuQAFs52EzItZy7mdIeuz4nJq1uthUR7X0hzIXpgqa7wM4TOEcPDvCMpUeHOGme4gOs16DBThJsf6fMuUJ2gqzsBHMY/ttVTYWx0ZWRfX2CTvUKRsYSFzKq3dKlWSicbS2IsXYB/W5XUkqSHJZBSZRUacKqTwi7avVpDCzp2NnhumANamrBVNKu5Hpwaaykl1HULirk/vXe/GLIRFYVWIbiCvSzyPRppBPVWlEDdoMGf66J4gqjkgfqmWXRyBVPUrU+Sg2bjrAv40OeIjVhAGAaKl5wn+V//SAxIMKMdJCt2gXBmB5zwVPM56NO8RgH+hPX2iZpTPnbCkNnFVhcgzruBT4Z7ul0dVOLnjRTZRr2TNBM3SbmhlXBQaM1AqIdDlt8MLcy6CkCcSXLsmU7dOledB9XeCbFkY7rlIjMDIPiqfSl1eHLrmI4j4Usmio2UjbhVFeYkJEBH71rOoOv6leubjEDMUDuuf222qlDY/wvjz4dmOIU394EpWdPmxRr1KaBrXRFxZ0Q29/7gwfWbOymjmwtL/Yl8G8yyXqDp0s3fDZmpz7QotYkK59M1A9HRKuD2897EeKMG9ldJJv6BAKDKHyk2Wz0BIflxNpEHPN+U2UPHLq/+ERp8OgcnRSGPvkXoace3Ll7+tiX4uCCvq9fLOFl+WBA7AsiwqjefSuNFa/xrYf0a";
    }
}
