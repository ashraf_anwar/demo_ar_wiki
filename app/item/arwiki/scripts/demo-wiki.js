var World = {
  loadContent: function loadContent(character) {
    var charimg = new AR.ImageResource(`../images/${character.name}.jpeg`);
    var charDrawable = new AR.ImageDrawable(charimg, 8, {
      zOrder: 1
    });

    // set indicator to the stop
    var indicatorImg = new AR.ImageResource("../../images/indi.png");
    var indicator = new AR.ImageDrawable(indicatorImg, 0.1, {
      verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP
    });

    // set location
    var chrLocation = new AR.RelativeLocation(null, 2, 2, 0);

    // set geoobject on its location
    var chrObject = new AR.GeoObject(chrLocation, {
      drawables: {
        cam: [charDrawable]
      },
      onClick: function() {
        charDrawable.enabled = false;
        console.log(`Get ${character}!!!`);
      }
    });
    // // add indicator to help locate drawables
    stopObject.drawables.addIndicatorDrawable(indicator);
  }
};