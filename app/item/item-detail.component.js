"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var nativescript_angular_1 = require("nativescript-angular");
var item_service_1 = require("./item.service");
var ItemDetailComponent = /** @class */ (function () {
    function ItemDetailComponent(itemService, route, router) {
        this.itemService = itemService;
        this.route = route;
        this.router = router;
    }
    ItemDetailComponent.prototype.ngOnInit = function () {
        var id = +this.route.snapshot.params["id"];
        this.item = this.itemService.getItem(id);
        this.wikitudeUrl = "./arwiki/index.html";
    };
    ItemDetailComponent.prototype.onWorldLoaded = function (wikitudeEvent) {
        global.WikitudeInstance = this.wikitude = wikitudeEvent.object;
        var arwikiCmd = "World.loadContent(" + JSON.stringify(this.item) + ")";
        this.wikitude.runJavascript(arwikiCmd);
    };
    ItemDetailComponent.prototype.onWorldFail = function (wikitudeEvent) {
        this.wikitude = wikitudeEvent.object;
    };
    ItemDetailComponent.prototype.goBack = function () {
        this.router.back();
    };
    ItemDetailComponent = __decorate([
        core_1.Component({
            selector: "ns-details",
            moduleId: module.id,
            templateUrl: "./item-detail.component.html",
        }),
        __metadata("design:paramtypes", [item_service_1.ItemService,
            router_1.ActivatedRoute,
            nativescript_angular_1.RouterExtensions])
    ], ItemDetailComponent);
    return ItemDetailComponent;
}());
exports.ItemDetailComponent = ItemDetailComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1kZXRhaWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbS1kZXRhaWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDBDQUFpRDtBQUVqRCw2REFBd0Q7QUFHeEQsK0NBQTZDO0FBTzdDO0lBS0ksNkJBQ1ksV0FBd0IsRUFDeEIsS0FBcUIsRUFDckIsTUFBd0I7UUFGeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7SUFDaEMsQ0FBQztJQUVMLHNDQUFRLEdBQVI7UUFDSSxJQUFNLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcscUJBQXFCLENBQUM7SUFDN0MsQ0FBQztJQUVELDJDQUFhLEdBQWIsVUFBYyxhQUFhO1FBQ3RCLE1BQWMsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFFeEUsSUFBTSxTQUFTLEdBQUcsdUJBQXFCLElBQUksQ0FBQyxTQUFTLENBQ25ELElBQUksQ0FBQyxJQUFJLENBQ1YsTUFBRyxDQUFDO1FBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELHlDQUFXLEdBQVgsVUFBWSxhQUFhO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQztJQUN6QyxDQUFDO0lBRUQsb0NBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQWhDUSxtQkFBbUI7UUFML0IsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsOEJBQThCO1NBQzlDLENBQUM7eUNBTzJCLDBCQUFXO1lBQ2pCLHVCQUFjO1lBQ2IsdUNBQWdCO09BUjNCLG1CQUFtQixDQWlDL0I7SUFBRCwwQkFBQztDQUFBLEFBakNELElBaUNDO0FBakNZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgV2lraXR1ZGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXdpa2l0dWRlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XG5cbmltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9pdGVtXCI7XG5pbXBvcnQgeyBJdGVtU2VydmljZSB9IGZyb20gXCIuL2l0ZW0uc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy1kZXRhaWxzXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2l0ZW0tZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXG59KVxuZXhwb3J0IGNsYXNzIEl0ZW1EZXRhaWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGl0ZW06IEl0ZW07XG4gICAgd2lraXR1ZGU6IFdpa2l0dWRlO1xuICAgIHdpa2l0dWRlVXJsOiBzdHJpbmc7XG4gIFxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGl0ZW1TZXJ2aWNlOiBJdGVtU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJFeHRlbnNpb25zXG4gICAgKSB7IH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICBjb25zdCBpZCA9ICt0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdO1xuICAgICAgICB0aGlzLml0ZW0gPSB0aGlzLml0ZW1TZXJ2aWNlLmdldEl0ZW0oaWQpO1xuICAgICAgICB0aGlzLndpa2l0dWRlVXJsID0gYC4vYXJ3aWtpL2luZGV4Lmh0bWxgO1xuICAgIH1cblxuICAgIG9uV29ybGRMb2FkZWQod2lraXR1ZGVFdmVudCkge1xuICAgICAgICAoZ2xvYmFsIGFzIGFueSkuV2lraXR1ZGVJbnN0YW5jZSA9IHRoaXMud2lraXR1ZGUgPSB3aWtpdHVkZUV2ZW50Lm9iamVjdDtcbiAgICBcbiAgICAgICAgY29uc3QgYXJ3aWtpQ21kID0gYFdvcmxkLmxvYWRDb250ZW50KCR7SlNPTi5zdHJpbmdpZnkoXG4gICAgICAgICAgdGhpcy5pdGVtXG4gICAgICAgICl9KWA7XG4gICAgICAgIHRoaXMud2lraXR1ZGUucnVuSmF2YXNjcmlwdChhcndpa2lDbWQpO1xuICAgIH1cbiAgICBcbiAgICBvbldvcmxkRmFpbCh3aWtpdHVkZUV2ZW50KSB7XG4gICAgICAgIHRoaXMud2lraXR1ZGUgPSB3aWtpdHVkZUV2ZW50Lm9iamVjdDtcbiAgICB9ICAgIFxuICAgIFxuICAgIGdvQmFjaygpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIuYmFjaygpO1xuICAgIH1cbn1cbiJdfQ==