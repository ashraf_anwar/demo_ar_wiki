import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Wikitude } from "nativescript-wikitude";
import { RouterExtensions } from "nativescript-angular";

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-details",
    moduleId: module.id,
    templateUrl: "./item-detail.component.html",
})
export class ItemDetailComponent implements OnInit {
    item: Item;
    wikitude: Wikitude;
    wikitudeUrl: string;
  
    constructor(
        private itemService: ItemService,
        private route: ActivatedRoute,
        private router: RouterExtensions
    ) { }

    ngOnInit(): void {
        const id = +this.route.snapshot.params["id"];
        this.item = this.itemService.getItem(id);
        this.wikitudeUrl = `./arwiki/index.html`;
    }

    onWorldLoaded(wikitudeEvent) {
        (global as any).WikitudeInstance = this.wikitude = wikitudeEvent.object;
    
        const arwikiCmd = `World.loadContent(${JSON.stringify(
          this.item
        )})`;
        this.wikitude.runJavascript(arwikiCmd);
    }
    
    onWorldFail(wikitudeEvent) {
        this.wikitude = wikitudeEvent.object;
    }    
    
    goBack() {
        this.router.back();
    }
}
